FROM ubuntu:20.04

ENV DEBIAN_FRONTEND=noninteractive
ENV HOME /root
ENV TZ=Australia/Brisbane
ENV LANG=C.UTF-8

RUN echo "export TZ=$TZ" >> $HOME/.bashrc
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN echo "export LANG=$LANG" >> $HOME/.bashrc
RUN mkdir -p $HOME/bin
RUN echo "export PATH=$HOME/bin:\$PATH" >> $HOME/.bashrc

RUN apt-get update \
    && \
    apt-get install -y --no-install-recommends \
    bash \
    build-essential \
    ca-certificates \
    curl \
    git \
    libffi-dev \
    libffi7 \
    libgmp-dev \
    libgmp10 \
    libncurses-dev \
    libncurses5\
    libtinfo5 \
    pkg-config \
    rsync \
    sudo \
    w3c-linkchecker \
    wget \
    zlib1g-dev
RUN curl --proto '=https' --tlsv1.2 -sSf https://get-ghcup.haskell.org | env BOOTSTRAP_HASKELL_NONINTERACTIVE=1 sh
RUN echo "export PATH=\$HOME/.cabal/bin:\$HOME/.ghcup/bin:\$PATH" >> $HOME/.bashrc
ENV PATH "${HOME}/.cabal/bin:${HOME}/.ghcup/bin:${PATH}"
RUN ghcup install ghc 9.6.5
RUN ghcup set ghc 9.6.5
RUN ghcup install cabal 3.10.2.0
RUN ghcup set cabal 3.10.2.0
RUN cabal update
RUN cabal install --lib --global filepath-1.4.300.2
RUN cabal install --lib --global lens-5.2.3
RUN cabal install --lib --global pandoc-3.1.13
RUN cabal install --lib --global pandoc-types-1.23.1
RUN cabal install --lib --global hakyll-4.16.2.0

# test commands
RUN echo $PATH
RUN cabal --version
# w3c-linkchecker
RUN checklink --version
RUN curl --version
RUN ghc --version
RUN ghcup --version
RUN git --version
RUN rsync --version
RUN wget --version

RUN git clone https://gitlab.com/system-f/systemf.com.au.git/ && cd systemf.com.au && cabal build
RUN git clone https://gitlab.com/squash-schedule/squash-schedule.git/ && cd squash-schedule && cabal build
