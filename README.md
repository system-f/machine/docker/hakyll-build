### Docker file reference

[https://docs.docker.com/engine/reference/builder/](https://docs.docker.com/engine/reference/builder/)

### Docker pruning

[https://docs.docker.com/config/pruning/](https://docs.docker.com/config/pruning/)

### Build Docker image and shell

`docker build -f Dockerfile --tag fred .`

`docker run -it fred /bin/bash`

### Build docker for repository

`docker build -f Dockerfile -t registry.gitlab.com/system-f/<repository> .`

### Push docker for repository

`docker push registry.gitlab.com/system-f/<repository>`

### Reference docker image in .gitlab-ci.yml

`image: registry.gitlab.com/system-f/<repository>:latest`

----

## Example

### Build `hakyll-build` docker image

`docker build -f Dockerfile -t registry.gitlab.com/system-f/machine/hakyll-build .`

### Shell to `hakyll-build` document image

`docker run -it registry.gitlab.com/system-f/machine/hakyll-build`

### Push `hakyll-build` docker image

`docker push registry.gitlab.com/system-f/machine/hakyll-build`
